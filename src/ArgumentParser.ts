import { Argument, ParameterDefinition, ParameterDefinitionValue } from "./Argument";


const parseTypedValue = (type: ParameterDefinitionValue, argument: string) => {
    switch (type) {
        case String:
            return argument
        case Number:
            const value = Number(argument)
            return isNaN(value) ? undefined : value
        default:
            throw new Error("Unknown Type")
    }
}

export type MatchedArguments = { [key: string]: any }

export class ArgumentParser {
    protected parsedArguments: Argument[]

    constructor(parameterDefinitions: ParameterDefinition[]) {
        this.parsedArguments = parameterDefinitions.map((definition) => new Argument(definition))
    }

    parse(args: string[]) {
        const matchedArgs: MatchedArguments = {}


        let currentArg = args.shift()
        if (currentArg !== undefined && this.parsedArguments.length) {
            do {
                if (!currentArg.startsWith('-')) {
                    args.unshift(currentArg)
                    break;
                }
                const isLongArgument = currentArg.startsWith('--')
                
                for (const parsedArgument of this.parsedArguments) {
                    const parsedArgumentAsString = parsedArgument.getLong(isLongArgument)

                    if (currentArg.trim() === parsedArgumentAsString) {
                        let value: any = true

                        const type = parsedArgument.getType()
                        if (type !== undefined) {
                            const argumentValue = args.shift()
                            if (argumentValue === undefined) {
                                throw new Error(`No value given for ${parsedArgumentAsString}`)
                            }
                            value = parseTypedValue(type, argumentValue)
                            if (value === undefined) {
                                throw new Error(`Invalid value "${argumentValue}" given for ${parsedArgumentAsString}`)
                            }
                        }
                        matchedArgs[parsedArgument.getName()] = value
                    }
                }
                currentArg = args.shift()
            } while (currentArg);
        }

        const matchedArgsNames = Object.keys(matchedArgs)

        return {
            arguments: args,
            matches: matchedArgs,
            allRequiredSet: this.areAllRequiredArgumentsMatched(matchedArgsNames)
        }
    }

    protected areAllRequiredArgumentsMatched(matchedArgsNames: string[]) {
        const requiredArgumentsNames = this.parsedArguments.reduce((carry, argument) => {
            if(argument.isRequired()) {
                carry.push(argument.getName())
            }
            return carry
        }, <string[]>[])

        for(const requiredName of requiredArgumentsNames) {
            if(!matchedArgsNames.includes(requiredName)) {
                return false
            }
        }

        return true
    }

    static PrintHelp(parameterDefinitions: ParameterDefinition[], name?: string, description?: string) {
        const Reset = "\x1b[0m"
        const Bright = "\x1b[1m"
        const Underscore = "\x1b[4m"
        console.log(`\t${Bright}${Underscore}${name}${Reset}`)
        console.log("")
        console.group(`${Underscore}Parameter${Reset}:`)
        for (const definition of parameterDefinitions) {
            console.group(`${Bright}${definition.name}${Reset}: ${definition.description}`)
            const long = definition.long ?? definition.name
            const short = definition.short ? ` | -${definition.short}` : '     '
            const type = definition.type ? `\tType: ${definition.type.name}` : ''
            console.log(`--${long}${short}${type}`)
            console.groupEnd()
        }
        console.groupEnd()
    }
}