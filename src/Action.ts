import { ParameterDefinition } from "./Argument";
import { ArgumentParser, MatchedArguments } from "./ArgumentParser";

export type ActionFunction = (matchedArguments: MatchedArguments, additionalArguments: string[] | undefined, subAction: Action | undefined) => Promise<any> | void | undefined

export class Action {

    protected name: string = ''
    protected argumentsDefinitions: ParameterDefinition[] = []
    protected actionFunction: ActionFunction = <any>undefined

    protected subActions: { [key: string]: Action } = {}

    constructor(name: string | undefined = undefined, argumentsDefinitions: ParameterDefinition[] | undefined = undefined, actionFunction: ActionFunction | undefined = undefined, subActions: Action[] = []) {
        if(name === undefined) {
            if(this.name === '') {
                this.name = this.constructor.name.toLowerCase().replace('action', '')
            }
        } else {
            this.name = name
        }

        if(argumentsDefinitions !== undefined) {
            this.argumentsDefinitions = argumentsDefinitions
        }

        if(actionFunction !== undefined) {
            this.actionFunction = actionFunction
        }

        for(const subAction of subActions) {
            this.addSubAction(subAction)
        }
    }

    getName() {
        return this.name
    }

    addSubAction(action: Action | typeof Action) {
        if(!(action instanceof Action)) {
            action = new action
        }
        this.subActions[action.getName()] = action
    }

    async run(args: string[]) {
        const argumentParser = new ArgumentParser(this.argumentsDefinitions)
        const result = argumentParser.parse(args)
        
        if(!result.allRequiredSet) {
            this.printHelp()
            return
        }
        
        const hasSubActions = this.hasSubActions()
        const additionalArguments = hasSubActions ? undefined : result.arguments        
        const subAction = hasSubActions && result.arguments.length > 0 ? this.subActions[<string>result.arguments.shift()] : undefined

        await this.actionFunction(result.matches, hasSubActions ? undefined : additionalArguments, subAction)

        if(subAction !== undefined) {
            await subAction.run(result.arguments)
        }
    }

    hasSubActions() {
        return Object.keys(this.subActions).length !== 0
    }


    getDefinition() {
        return this.argumentsDefinitions
    }

    printHelp() {
        ArgumentParser.PrintHelp(this.argumentsDefinitions, this.name)
    }
}