import { ArgumentParser } from "./ArgumentParser"
import { Action, ActionFunction, ParameterDefinition } from "./lib"

const definitionsMain: ParameterDefinition[] = [
    { name: "library", description: "Set library", required: true},
    { name: "help", description: "Prints help screen" },
    { name: "verbose", description: "Verbose output", short: 'v' }
]

const args = process.argv.slice(2)

const mainAction = new Action('main', definitionsMain, (matchedArgs, additionalArguments, subAction) => {
    // console.log("main", matchedArgs, additionalArguments)
    // console.log("will call subAction: ", subAction?.getName())
})


class SearchAction extends Action {
    protected argumentsDefinitions: ParameterDefinition[] = [
        { name: "help", description: "Prints help screen" },
        { name: "path", description: "Path to search in ", type: String, required: true },
    ]

    protected actionFunction: ActionFunction = (matchedArgs, additionalArguments) => {
        console.log("search", matchedArgs, additionalArguments)
    }
}

mainAction.addSubAction(SearchAction)
mainAction.run(args)