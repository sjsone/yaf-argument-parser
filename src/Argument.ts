export type ParameterDefinitionValue = typeof String | typeof Number | typeof Boolean

export interface ParameterDefinition {
    name: string
    description: string
    short?: string,
    multiple?: boolean,
    type?: ParameterDefinitionValue
    long?: string,
    boolean?: boolean
    help?: boolean
    required?: boolean
}


export class Argument {
    protected name: string
    protected description: string
    protected short?: string | undefined
    protected multiple?: boolean | undefined
    protected long: string
    protected boolean?: boolean | undefined
    protected type?: ParameterDefinitionValue | undefined
    protected help?: boolean
    protected required?: boolean

    constructor(definition: ParameterDefinition) {
        this.name = definition.name
        this.description = definition.description
        this.short = definition.short
        this.multiple = definition.multiple
        this.long = definition.long ?? definition.name
        this.boolean = definition.boolean
        this.type = definition.type
        this.help = definition.help
        this.required = definition.required
    }

    public getName() {
        return this.name
    }

    public hasShort() {
        return this.short !== undefined
    }

    public hasMultiple() {
        return this.multiple !== undefined
    }

    public isBoolean() {
        return this.boolean !== undefined
    }

    public hasType() {
        return this.type !== undefined
    }

    public getLong(withPrefix = false) {
        return (withPrefix ? '--' : '') + this.long
    }

    public getType() {
        return this.type
    }

    public isHelp() {
        return this.help === true
    }

    public isRequired() {
        return this.required === true
    }

}