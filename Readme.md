# Yet Another F@#%ing Argument Parser
A simple argument parsing library with nesting actions support. Each having it own set of parameter definitions.
## Simple Example
```ts
import { ParameterDefinition, ArgumentParser } from "yaf-argument-parser"

const definitions: ParameterDefinition[] = [
    { name: "help", description: "", long: 'help' },
    { name: "testString", description: "", long: 'tstring', type: String },
    { name: "testNumber", description: "", long: 'tnumber', type: Number },
]
const args = process.argv.slice(2)

const argumentParser = new ArgumentParser(definitions)
console.log(argumentParser.parse(args))
```

```shell
node example.js
# --> { arguments: [], matches: {} }
node example.js --help
# --> { arguments: [], matches: { help: true } }
node out/test.js --tstring "bla blu" --tnumber 432
# --> { arguments: [], matches: { testString: 'bla blu', testNumber: 432 } }
```


## Actions

### Examples

```ts
import { Action, ParameterDefinition } from "yaf-argument-parser"

const definitionsMain: ParameterDefinition[] = [
    { name: "help", description: "" },
    { name: "verbose", description: "" }
]

const definitionsSearch: ParameterDefinition[] = [
    { name: "help", description: "" },
    { name: "path", description: "", type: String },
]

const args = process.argv.slice(2)

const mainAction = new Action('main', definitionsMain, (matchedArgs, additionalArguments) => {
    console.log("main", matchedArgs, additionalArguments)
})

const searchAction = new Action('search', definitionsSearch, (matchedArgs, additionalArguments) => {
    console.log("search", matchedArgs, additionalArguments)
})

mainAction.addSubAction(searchAction)
mainAction.run(args)
```

```shell
node example.js --verbose search --path "/home/user/"
# --> main { verbose: true } undefined
# --> search { path: '/home/user/' } []
```
